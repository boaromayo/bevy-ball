use bevy::prelude::*;

use crate::main_menu::components::*;
use crate::main_menu::styles::*;

pub fn spawn_main_menu(mut commands: Commands,
    asset_server: Res<AssetServer>) {

    let main_menu_entity = build_main_menu(&mut commands, &asset_server);
}

pub fn despawn_main_menu(mut commands: Commands,
    main_menu_query: Query<Entity, With<MainMenu>>) {

    // Clear out UI entity and all its sub-components
    if let Ok(main_menu_entity) = main_menu_query.get_single() {
        commands.entity(main_menu_entity).despawn_recursive();
    }
}

pub fn build_main_menu(commands: &mut Commands,
    asset_server: &Res<AssetServer>) -> Entity {

    // Create main bundle and main menu components
    let main_menu_bundle = NodeBundle {
        style: MAIN_MENU_STYLE,
        ..default()
    };
    let main_menu = MainMenu {};

    let main_menu_entity = commands.spawn((main_menu_bundle, main_menu))
        .with_children(|parent| {
            // Nest child UI nodes/components here
            // In this case, create the title and main menu buttons
            // Hold on to keys and wigs, this is a long process
            // === Title ===
            let title_bundle = NodeBundle {
                style: TITLE_STYLE,
                ..default()
            };
            parent
                .spawn(title_bundle)
                .with_children(|parent| {
                    // === Left Image ===
                    let left_image = ImageBundle {
                        style: IMAGE_STYLE,
                        image: asset_server.load("sprites/big_blue_ball.png").into(),
                        ..default()
                    };
                    parent.spawn(left_image);
                    // === Title Text ===
                    // Hold on to anything, the title text bundle is here
                    let title_text = TextBundle {
                        // Make the title text
                        text: Text {
                            sections: vec![
                                // Where the actual title text is
                                TextSection::new(
                                    "Bevy Ball Game",
                                    get_title_text_style(&asset_server),
                                )
                            ],
                            alignment: TextAlignment::Center,
                            ..default()
                        },
                        ..default()
                    };
                    parent.spawn(title_text);
                    // === Right Image ===
                    let right_image = ImageBundle {
                        style: IMAGE_STYLE,
                        image: asset_server.load("sprites/big_red_ball.png").into(),
                        ..default()
                    };
                    parent.spawn(right_image);
                }
            );
            // === Play Button ===
            let play_button_bundle = ButtonBundle {
                style: BUTTON_STYLE,
                background_color: NORMAL_BUTTON_COLOR.into(),
                ..default()
            };
            let play_button = PlayButton {};
            parent
                .spawn((play_button_bundle, play_button))
                .with_children(|parent| {
                    // Add play text to button
                    // Buckle up, assembling the TextBundle is a multi-layered process
                    let play_text = TextBundle {
                        // Make the Text component
                        text: Text {
                            sections: vec![
                                // Where the actual text and how it looks is
                                TextSection::new(
                                    "Play",
                                    get_button_text_style(&asset_server),
                                )
                            ],
                            alignment: TextAlignment::Center,
                            ..default()
                        },
                        ..default() 
                    };
                    parent.spawn(play_text);
                }
            );
            // === Quit Button ===
            let quit_button_bundle = ButtonBundle {
                style: BUTTON_STYLE,
                background_color: NORMAL_BUTTON_COLOR.into(),
                ..default()
            };
            let quit_button = QuitButton {};
            parent
                .spawn((quit_button_bundle, quit_button))
                .with_children(|parent| {
                    // Add quit text to button
                    // Buckle up, like before, assembling the TextBundle is a multi-layered process
                    let quit_text = TextBundle {
                        // Make the Text component
                        text: Text {
                            sections: vec![
                                // Where the actual text and how it looks is
                                TextSection::new(
                                    "Quit",
                                    get_button_text_style(&asset_server),
                                )
                            ],
                            alignment: TextAlignment::Center,
                            ..default()
                        },
                        ..default() 
                    };
                    parent.spawn(quit_text);
                }
            );
        })
        .id();

    return main_menu_entity;
}