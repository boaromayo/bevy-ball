mod components;
mod styles;
mod systems;

use bevy::prelude::*;
use crate::AppState;

use systems::interactions::*;
use systems::layout::*;

pub struct MainMenuPlugin;

// Implement plugin build for main menu
impl Plugin for MainMenuPlugin {
    fn build(&self, app: &mut App) {
        app
        // Enter state system
        .add_system(spawn_main_menu.in_schedule(OnEnter(AppState::MainMenu)))
        // Additional systems
        // Button interaction
        .add_systems(
            (interact_with_play_button, interact_with_quit_button)
                .in_set(OnUpdate(AppState::MainMenu))
        )
        // Exit state system
        .add_system(despawn_main_menu.in_schedule(OnExit(AppState::MainMenu)));
    }
}