use bevy::app::AppExit;
use bevy::prelude::*;
use bevy::window::PrimaryWindow;

// Call other files
use crate::AppState;
use crate::events::*;

// Create camera bundle system
pub fn spawn_camera(mut commands: Commands, 
    window_query: Query<&Window, With<PrimaryWindow>>) {

    let window = window_query.get_single().unwrap();

    commands.spawn(Camera2dBundle {
        transform: Transform::from_xyz(window.width() / 2.0, window.height() / 2.0, 0.0),
        ..default()
    });
}

// State transition systems
pub fn transition_to_game_state(keyboard_input: Res<Input<KeyCode>>,
    app_state: Res<State<AppState>>,
    mut next_app_state: ResMut<NextState<AppState>>) {

    if keyboard_input.just_pressed(KeyCode::G) {
        if app_state.0 != AppState::Game {
            next_app_state.set(AppState::Game); // Preferred method is to use next_app_state.set()
            println!("Entered AppState::Game");
        }
    }
}

pub fn transition_to_main_menu_state(mut commands: Commands,
    keyboard_input: Res<Input<KeyCode>>,
    app_state: Res<State<AppState>>) {

    if keyboard_input.just_pressed(KeyCode::M) {
        if app_state.0 != AppState::MainMenu {
            commands.insert_resource(NextState(Some(AppState::MainMenu)));
            println!("Entered AppState::MainMenu");
        }
    }
}

// Temporary game over call event system
pub fn handle_game_over(mut game_over_event_reader: EventReader<GameOver>) {
    for event in game_over_event_reader.iter() {
        println!("Final score: {}", event.score.to_string());
    }
}

// Exit game system
pub fn exit_game(keyboard_input: Res<Input<KeyCode>>, 
    mut app_exit_event_writer: EventWriter<AppExit>,) {

    // Press Escape to exit
    if keyboard_input.just_pressed(KeyCode::Escape) {
        app_exit_event_writer.send(AppExit);
    }
}