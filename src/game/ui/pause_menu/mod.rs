mod components;
mod styles;
mod systems;

use bevy::prelude::*;
use crate::game::RunState;

use systems::interactions::*;
use systems::layout::*;

pub struct PauseMenuPlugin;

// Implement plugin for pause menu
impl Plugin for PauseMenuPlugin {
    fn build(&self, app: &mut App) {
        app
        // Enter state system
        .add_system(spawn_pause_menu.in_schedule(OnEnter(RunState::Paused)))
        // Additional systems to load
        .add_systems(
            (
                interact_with_resume_button,
                interact_with_main_menu_button,
                interact_with_quit_button,
            )
                .in_set(OnUpdate(RunState::Paused))
        )
        // Exit state system
        .add_system(despawn_pause_menu.in_schedule(OnExit(RunState::Paused)));
    }
}