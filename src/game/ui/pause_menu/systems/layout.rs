use bevy::prelude::*;

use crate::game::ui::pause_menu::components::*;
use crate::game::ui::pause_menu::styles::*;

pub fn spawn_pause_menu(mut commands: Commands,
    asset_server: Res<AssetServer>) {

    let _pause_menu_entity = build_pause_menu(&mut commands, &asset_server);
}

pub fn despawn_pause_menu(mut commands: Commands,
    pause_menu_query: Query<Entity, With<PauseMenu>>) {

    // Clear out UI entity and its sub-components
    if let Ok(pause_menu_entity) = pause_menu_query.get_single() {
        commands.entity(pause_menu_entity).despawn_recursive();
    }
}

pub fn build_pause_menu(commands: &mut Commands,
    asset_server: &Res<AssetServer>) -> Entity {

    // Create pause menu bundle and its components
    let pause_menu_bundle = NodeBundle {
        style: PAUSE_MENU_STYLE,
        ..default()
    };
    let pause_menu = PauseMenu {};

    let pause_menu_entity = commands.spawn((pause_menu_bundle, pause_menu))
        .with_children(|parent| {
            // Nest UI child nodes/components here
            // Create the pause menu components
            // === Pause Title ===
            let pause_title_bundle = NodeBundle {
                style: TITLE_STYLE,
                background_color: BACKGROUND_COLOR.into(),
                ..default()
            };
            // Create the pause title
            parent
                .spawn(pause_title_bundle)
                .with_children(|parent| {
                    // === Pause Text ===
                    let pause_text = TextBundle {
                        // Make pause text
                        text: Text {
                            sections: vec![
                                create_text("Game Paused", get_title_text_style(&asset_server))
                            ],
                            alignment: TextAlignment::Center,
                            ..default()
                        },
                        ..default()
                    };
                    parent.spawn(pause_text);
                }
            );
            // === Resume Button ===
            let resume_button_bundle = ButtonBundle {
                style: BUTTON_STYLE,
                background_color: NORMAL_BUTTON_COLOR.into(),
                ..default()
            };
            let resume_button = ResumeButton {};
            // Create resume button
            parent
                .spawn((resume_button_bundle, resume_button))
                .with_children(|parent| {
                    // === Resume Text ===
                    let resume_text = TextBundle {
                        // Make resume button text
                        text: Text {
                            sections: vec![
                                create_text("Resume", get_button_text_style(&asset_server))
                            ],
                            alignment: TextAlignment::Center,
                            ..default()
                        },
                        ..default()
                    };
                    parent.spawn(resume_text);
                }
            );
            // === Main Menu Button ===
            let main_menu_button_bundle = ButtonBundle {
                style: BUTTON_STYLE,
                background_color: NORMAL_BUTTON_COLOR.into(),
                ..default()
            };
            let main_menu_button = MainMenuButton {};
            // Create main menu button
            parent
                .spawn((main_menu_button_bundle, main_menu_button))
                .with_children(|parent| {
                    // === Main Menu Text ===
                    let main_menu_text = TextBundle {
                        // Make main menu button text
                        text: Text {
                            sections: vec![
                                create_text("Main Menu", get_button_text_style(&asset_server))
                            ],
                            alignment: TextAlignment::Center,
                            ..default()
                        },
                        ..default()
                    };
                    parent.spawn(main_menu_text);
                }
            );
            // === Quit Button ===
            let quit_button_bundle = ButtonBundle {
                style: BUTTON_STYLE,
                background_color: NORMAL_BUTTON_COLOR.into(),
                ..default()
            };
            let quit_button = QuitButton {};
            // Create quit button
            parent
                .spawn((quit_button_bundle, quit_button))
                .with_children(|parent| {
                    // === Quit Text ===
                    let quit_text = TextBundle {
                        // Make quit button text
                        text: Text {
                            sections: vec![
                                create_text("Quit", get_button_text_style(&asset_server))
                            ],
                            alignment: TextAlignment::Center,
                            ..default()
                        },
                        ..default()
                    };
                    parent.spawn(quit_text);
                }
            );
        }
    ).id();

    return pause_menu_entity;
}
