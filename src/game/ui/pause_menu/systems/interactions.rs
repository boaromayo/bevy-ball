use bevy::app::AppExit;
use bevy::prelude::*;

use crate::AppState;
use crate::game::RunState;

use crate::game::ui::pause_menu::components::*;
use crate::game::ui::pause_menu::styles::*;

pub fn interact_with_resume_button(
    mut app_state_next_state: ResMut<NextState<RunState>>,
    mut button_query: Query<
        (&Interaction, &mut BackgroundColor),
        (Changed<Interaction>, With<ResumeButton>)
        >
) {

    if let Ok((interaction,  
            mut background_color)) = button_query.get_single_mut() {

        match *interaction {
            Interaction::Clicked => {
                *background_color = RESUME_PRESSED_BUTTON_COLOR.into();
                app_state_next_state.set(RunState::Running);
            },
            Interaction::Hovered => {
                *background_color = HOVERED_BUTTON_COLOR.into();
            },
            Interaction::None => {
                *background_color = NORMAL_BUTTON_COLOR.into();
            }
        };
    }
}

pub fn interact_with_main_menu_button(
    mut app_state_next_state: ResMut<NextState<AppState>>,
    mut button_query: Query<
        (&Interaction, &mut BackgroundColor),
        (Changed<Interaction>, With<MainMenuButton>)
        >
)   {

    if let Ok((interaction, 
            mut background_color)) = button_query.get_single_mut() {

        match *interaction {
            Interaction::Clicked => {
                *background_color = PRESSED_BUTTON_COLOR.into();
                app_state_next_state.set(AppState::MainMenu);
            },
            Interaction::Hovered => {
                *background_color = HOVERED_BUTTON_COLOR.into();
            },
            Interaction::None => {
                *background_color = NORMAL_BUTTON_COLOR.into();
            }
        };
    }
}

pub fn interact_with_quit_button(
    mut app_exit_event_writer: EventWriter<AppExit>,
    mut button_query: Query<
        (&Interaction, &mut BackgroundColor),
        (Changed<Interaction>, With<QuitButton>)
        >
)   {

    if let Ok((interaction, 
            mut background_color)) = button_query.get_single_mut() {

        match *interaction {
            Interaction::Clicked => {
                *background_color = QUIT_PRESSED_BUTTON_COLOR.into();
                app_exit_event_writer.send(AppExit);
            },
            Interaction::Hovered => {
                *background_color = HOVERED_BUTTON_COLOR.into();
            },
            Interaction::None => {
                *background_color = NORMAL_BUTTON_COLOR.into();
            }
        };
    }
}