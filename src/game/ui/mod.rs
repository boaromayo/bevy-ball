mod game_over_menu;
mod hud;
mod pause_menu;

use pause_menu::PauseMenuPlugin;

use bevy::prelude::*;

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app
            // Add in UI plugins
            //.add_plugin(HudPlugin)
            //.add_plugin(GameOverMenuPlugin)
            .add_plugin(PauseMenuPlugin);
    }
}
