use bevy::prelude::*;
use bevy::window::PrimaryWindow;

// Modules
use crate::game::player::components::Player;
use crate::game::player::{PLAYER_SIZE, PLAYER_SPEED};
use crate::game::enemy::components::*;
use crate::game::enemy::ENEMY_SIZE;
use crate::events::GameOver;
use crate::game::score::resources::*;
use crate::game::star::components::*;
use crate::game::star::STAR_SIZE;

// Create a player spawn system from bundle
pub fn spawn_player(mut commands: Commands, 
    window_query: Query<&Window, With<PrimaryWindow>>, 
    asset_server: Res<AssetServer>) {

    // fetch a single window from the window query
    let window = window_query.get_single().unwrap();

    // Load bundles for sprite and player
    let sprite_bundle = SpriteBundle {
        transform: Transform::from_xyz(window.width() / 2.0, window.height() / 2.0, 0.0),
        texture: asset_server.load("sprites/big_blue_ball.png"),
        ..default()
    };
    let player = Player {};

    commands.spawn(
        (sprite_bundle, player)
    );
}

// Remove a player
pub fn despawn_player(mut commands: Commands,
    player_query: Query<Entity, With<Player>>) {

    if let Ok(player_entity) = player_query.get_single() {
        commands.entity(player_entity).despawn();
    }
}

// Create player movement system
pub fn player_movement(keyboard_input: Res<Input<KeyCode>>, 
    mut player_query: Query<&mut Transform, With<Player>>, 
    time: Res<Time>) {
    
    // Keyboard inputs listed here
    if let Ok(mut player_transform) = player_query.get_single_mut() {
        let mut direction = Vec3::ZERO;

        // Left or A
        if keyboard_input.pressed(KeyCode::Left) || keyboard_input.pressed(KeyCode::A) {
            direction += Vec3::new(-1.0, 0.0, 0.0);
        }
        // Right or D
        if keyboard_input.pressed(KeyCode::Right) || keyboard_input.pressed(KeyCode::D) {
            direction += Vec3::new(1.0, 0.0, 0.0);
        }
        // Up or W
        if keyboard_input.pressed(KeyCode::Up) || keyboard_input.pressed(KeyCode::W) {
            direction += Vec3::new(0.0, 1.0, 0.0);
        }
        // Down or S
        if keyboard_input.pressed(KeyCode::Down) || keyboard_input.pressed(KeyCode::S) {
            direction += Vec3::new(0.0, -1.0, 0.0);
        }

        // Normalize input to ensure diagonals faster than horizontal or vertical movement
        if direction.length() > 0.0 {
            direction = direction.normalize();
        }

        // Move player
        player_transform.translation += direction * PLAYER_SPEED * time.delta_seconds();
    }
}

// Create player movement bounds system
pub fn confine_player_movement(mut player_query: Query<&mut Transform, With<Player>>, 
    window_query: Query<&Window, With<PrimaryWindow>>) {

    if let Ok(mut player_transform) = player_query.get_single_mut() {
        let window = window_query.get_single().unwrap();

        let half_player_size: f32 = PLAYER_SIZE / 2.0; // 32.0
        let x_min: f32 = 0.0 + half_player_size;
        let x_max: f32 = window.width() - half_player_size;
        let y_min: f32 = 0.0 + half_player_size;
        let y_max: f32 = window.height() - half_player_size;

        // Player translation vector
        let mut translation: Vec3 = player_transform.translation;

        // Bound the player x-position
        if translation.x < x_min {
            translation.x = x_min;
        } else if translation.x > x_max {
            translation.x = x_max;
        }
        // Bound the player y-position
        if translation.y < y_min {
            translation.y = y_min;
        } else if translation.y > y_max {
            translation.y = y_max;
        }

        // Set player position to new translation
        player_transform.translation = translation;
    }
}

// Create player-enemy collision system
pub fn enemy_hit_player(mut commands: Commands,
    mut game_over_event_writer: EventWriter<GameOver>, 
    mut player_query: Query<(Entity, &Transform), With<Player>>,
    enemy_query: Query<&Transform, With<Enemy>>,
    audio: Res<Audio>,
    asset_server: Res<AssetServer>,
    score: Res<Score>) {

    if let Ok((player_entity, player_transform)) = player_query.get_single_mut() {
        for enemy_transform in enemy_query.iter() {
            let distance = player_transform
            .translation
            .distance(enemy_transform.translation);

            let player_radius = PLAYER_SIZE / 2.0;
            let enemy_radius = ENEMY_SIZE / 2.0;

            if distance < player_radius + enemy_radius {
                println!("Enemy hit player! Game Over!");
                let sound_effect = asset_server.load("sounds/close_002.ogg");
                audio.play(sound_effect);
                commands.entity(player_entity).despawn();
                game_over_event_writer.send(GameOver { score: score.value });
            }
        }
    }
}

// Create player-collectible collision system
pub fn player_hit_star(mut commands: Commands,
    player_query: Query<&Transform, With<Player>>,
    star_query: Query<(Entity, &Transform), With<Star>>,
    audio: Res<Audio>,
    asset_server: Res<AssetServer>,
    mut score: ResMut<Score>) {

    if let Ok(player_transform) = player_query.get_single() {
        for (star_entity, star_transform) in star_query.iter() {
            let distance = player_transform
                .translation
                .distance(star_transform.translation);

            let player_radius = PLAYER_SIZE / 2.0;
            let star_radius = STAR_SIZE / 2.0;

            if distance < player_radius + star_radius {
                // A star nets a point to player
                score.value += 1;
                let sound_effect = 
                    asset_server.load("sounds/confirmation_002.ogg");
                audio.play(sound_effect);
                commands.entity(star_entity).despawn();
            }
        }
    }
}