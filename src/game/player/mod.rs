use bevy::prelude::*;

pub mod components;
mod systems;

use systems::*;

use crate::AppState;
use crate::game::RunState;

// System sets
// The struct way or the enum way
// The struct way:
// Uncomment and use `app.configure_set(MovementSystemSet.before(ConfinementSystemSet))`
// then `.add_system(player_movement.in_set(PlayerMovementSet))`
// and `.add_system(confine_player_movement.in_set(ConfinementMovementSet))
// to execute the system set below
#[derive(SystemSet, Debug, Hash, PartialEq, Eq, Clone)]
pub struct MovementSystemSet;

#[derive(SystemSet, Debug, Hash, PartialEq, Eq, Clone)]
pub struct ConfinementSystemSet;

// Using enums as system sets works too:
// Use `app.configure_set(PlayerSystemSet::Movement.before(PlayerSystemSet::Confinement))
// then `.add_system(player_movement.in_set(PlayerSystemSet::Movement))
// and `.add_system(confine_player_movement.in_set(PlayerSystemSet::Confinement))
// to execute this enum-based system set
/*#[derive(SystemSet, Debug, Hash, PartialEq, Eq, Clone)]
pub enum PlayerSystemSet {
    Movement,
    Confinement,
}*/

// Constants
pub const PLAYER_SIZE: f32 = 64.0; // Player sprite size
pub const PLAYER_SPEED: f32 = 500.0;

pub struct PlayerPlugin;

// Implement plugin traits for player
impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        // Load player systems
        app
            // Add enter state system
            //.add_startup_system(spawn_player)
            .add_system(spawn_player.in_schedule(OnEnter(AppState::Game)))
            //.add_system(player_movement.before(confine_player_movement)) <- Can also be used instead of .after()
            .add_systems(
                (
                    player_movement.in_set(MovementSystemSet),
                    confine_player_movement.in_set(ConfinementSystemSet)
                )
                .in_set(OnUpdate(AppState::Game))
                .in_set(OnUpdate(RunState::Running)),
            )
            .add_systems(
                (
                    enemy_hit_player,
                    player_hit_star,
                )
                .in_set(OnUpdate(AppState::Game))
                .in_set(OnUpdate(RunState::Running))
            )
            // Add exit state system
            .add_system(despawn_player.in_schedule(OnExit(AppState::Game)));
    }
}