use bevy::prelude::*;

// Enemy component
#[derive(Component)]
pub struct Enemy {
    pub direction: Vec2,
}