use bevy::prelude::*;

pub mod components;
pub mod resources;
mod systems;

use resources::*;
use systems::*;

use crate::AppState;
use crate::game::RunState;

// Constants
pub const NUM_OF_ENEMIES: usize = 4;
pub const ENEMY_SIZE: f32 = 64.0; // Enemy sprite size
pub const ENEMY_SPEED: f32 = 200.0;

pub struct EnemyPlugin;

// Implement plugin traits for enemy
impl Plugin for EnemyPlugin {
    fn build(&self, app: &mut App) {
        // Load enemy resources and systems
        app.init_resource::<EnemySpawnTimer>()
            // Add enter startup system
            //.add_startup_system(spawn_enemies)
            .add_system(spawn_enemies.in_schedule(OnEnter(AppState::Game)))
            // Add systems, per-frame update
            /*.add_system(enemy_movement)
            .add_system(update_enemy_movement)
            .add_system(tick_spawn_enemy_timer)
            .add_system(spawn_enemies_over_time)*/
            .add_systems(
                (
                    enemy_movement,
                    update_enemy_movement,
                    tick_spawn_enemy_timer,
                    spawn_enemies_over_time,
                )
                .in_set(OnUpdate(AppState::Game))
                .in_set(OnUpdate(RunState::Running))
            )
            // Add exit state system
            .add_system(despawn_enemies.in_schedule(OnExit(AppState::Game)));
    }
}