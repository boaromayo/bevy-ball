use bevy::prelude::*;
use bevy::window::PrimaryWindow;
use rand::prelude::*;

// Load modules
use crate::game::enemy::components::*;
use crate::game::enemy::resources::*;
use crate::game::enemy::{NUM_OF_ENEMIES, ENEMY_SIZE, ENEMY_SPEED};

// Create enemy spawn system
pub fn spawn_enemies(mut commands: Commands,
    window_query: Query<&Window, With<PrimaryWindow>>,
    asset_server: Res<AssetServer>) {

    let window = window_query.get_single().unwrap();

    for _ in 0..NUM_OF_ENEMIES {
        let rand_x: f32 = random::<f32>() * window.width();
        let rand_y: f32 = random::<f32>() * window.height();

        let sprite_bundle = SpriteBundle {
            transform: Transform::from_xyz(rand_x, rand_y, 0.0),
            texture: asset_server.load("sprites/big_red_ball.png"),
            ..default()
        };
        let enemy = Enemy {
            direction: Vec2::new(random::<f32>(), random::<f32>()).normalize(),
        };

        commands.spawn(
            (sprite_bundle, enemy)
        );
    }
}

// Despawn enemy spawn system
pub fn despawn_enemies(mut commands: Commands,
    enemy_query: Query<Entity, With<Enemy>>) {

    for enemy_entity in enemy_query.iter() {
        commands.entity(enemy_entity).despawn();
    }
}

// Create enemy movement system
pub fn enemy_movement(mut enemy_query: Query<(&mut Transform, &Enemy)>, 
    time: Res<Time>) {

    for (mut enemy_transform, enemy) in enemy_query.iter_mut() {
        // For easy calculation set direction vector to a 3-D than a 2-D one
        let direction = Vec3::new(enemy.direction.x, enemy.direction.y, 0.0);
        // Move enemy
        enemy_transform.translation += direction * ENEMY_SPEED * time.delta_seconds();
    }
}

// Create enemy movement bounds system
pub fn update_enemy_movement(mut enemy_query: Query<(&mut Transform, &mut Enemy), With<Enemy>>, 
    window_query: Query<&Window, With<PrimaryWindow>>,
    audio: Res<Audio>,
    asset_server: Res<AssetServer>) {

    let window = window_query.get_single().unwrap();

    let half_player_size: f32 = ENEMY_SIZE / 2.0; // 64.0
    let x_min: f32 = 0.0 + half_player_size;
    let x_max: f32 = window.width() - half_player_size;
    let y_min: f32 = 0.0 + half_player_size;
    let y_max: f32 = window.height() - half_player_size;

    for (mut enemy_transform, mut enemy) in enemy_query.iter_mut() {
        let mut translation = enemy_transform.translation;
        let mut direction_changed = false;

        // Change direction if hitting bounds
        if translation.x < x_min || translation.x > x_max {
            enemy.direction.x *= -1.0;
            direction_changed = true
        }
        if translation.y < y_min || translation.y > y_max {
            enemy.direction.y *= -1.0;
            direction_changed = true;
        }

        // Bound enemy x-position
        if translation.x < x_min {
            translation.x = x_min;
        } else if translation.x > x_max {
            translation.x = x_max;
        }
        // Bound enemy y-position
        if translation.y < y_min {
            translation.y = y_min;
        } else if translation.y > y_max {
            translation.y = y_max;
        }

        // Play SFX if bounds hit
        if direction_changed {
            let sound_effect_1 = asset_server.load("sounds/drop_002.ogg");
            let sound_effect_2 = asset_server.load("sounds/drop_004.ogg");
            let sound_effect = if random::<f32>() > 0.5 {
                sound_effect_1
            } else {
                sound_effect_2
            };
            audio.play(sound_effect);
        }
        // Set enemy position to new translation vector
        enemy_transform.translation = translation;
    }
}

// Tick enemy timer system
pub fn tick_spawn_enemy_timer(mut enemy_spawn_timer: ResMut<EnemySpawnTimer>, time: Res<Time>) {
    enemy_spawn_timer.timer.tick(time.delta());
}

// Spawn enemies over time system
pub fn spawn_enemies_over_time(mut commands: Commands, 
    window_query: Query<&Window, With<PrimaryWindow>>, 
    asset_server: Res<AssetServer>, 
    enemy_spawn_timer: Res<EnemySpawnTimer>) {

    // Spawn timer resets, so when it hits 0, spawn more enemies
    if enemy_spawn_timer.timer.finished() {
        let window = window_query.get_single().unwrap();
        let rand_x = random::<f32>() * window.width();
        let rand_y = random::<f32>() * window.height();

        let sprite_bundle = SpriteBundle {
            transform: Transform::from_xyz(rand_x, rand_y, 0.0),
            texture: asset_server.load("sprites/big_red_ball.png"),
            ..default()
        };
        let new_enemy = Enemy {
            direction: Vec2::new(random::<f32>(), random::<f32>()).normalize(),
        };

        commands.spawn(
            (sprite_bundle, new_enemy)
        );
    }
}