use bevy::prelude::*;

pub mod components;
mod resources;
mod systems;

use resources::*;
use systems::*;

use crate::AppState;
use crate::game::RunState;

// Constants
pub const NUM_OF_STARS: usize = 10;
pub const STAR_SIZE: f32 = 32.0; // Star sprite size

pub struct StarPlugin;

// Implement plugin traits for star
impl Plugin for StarPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<StarSpawnTimer>()
            // Add enter state system
            //.add_startup_system(spawn_stars)
            .add_system(spawn_stars.in_schedule(OnEnter(AppState::Game)))
            // Add systems
            //.add_system(tick_spawn_star_timer)
            //.add_system(spawn_stars_over_time);
            .add_systems(
                (
                    tick_spawn_star_timer,
                    spawn_stars_over_time,
                )
                .in_set(OnUpdate(AppState::Game))
                .in_set(OnUpdate(RunState::Running)),
            )
            // Add exit state system
            .add_system(despawn_stars.in_schedule(OnExit(AppState::Game)));
    }
}