use bevy::prelude::*;

pub const STAR_SPAWN_TIME: f32 = 1.0; // Time for new stars to spawn

// Star spawn timer resource
#[derive(Resource)]
pub struct StarSpawnTimer {
    pub timer: Timer,
}

// Implement default trait for star spawn timer
impl Default for StarSpawnTimer {
    fn default() -> StarSpawnTimer {
        StarSpawnTimer {
            timer: Timer::from_seconds(STAR_SPAWN_TIME, TimerMode::Repeating)
        }
    }
}