use bevy::prelude::*;
use bevy::window::PrimaryWindow;
use rand::prelude::*;

// Call modules
use crate::game::star::components::*;
use crate::game::star::resources::*;
use crate::game::star::NUM_OF_STARS;

// Create stars spawn system from bundle
pub fn spawn_stars(mut commands: Commands, 
    window_query: Query<&Window, With<PrimaryWindow>>, 
    asset_server: Res<AssetServer>) {

    let window = window_query.get_single().unwrap();

    for _ in 0..NUM_OF_STARS {
        let rand_x = random::<f32>() * window.width();
        let rand_y = random::<f32>() * window.height();

        let sprite_bundle = SpriteBundle {
            transform: Transform::from_xyz(rand_x, rand_y, 0.0),
            texture: asset_server.load("sprites/big_star.png"),
            ..default()
        };
        let star = Star {};

        commands.spawn(
            (sprite_bundle, star)
        );
    }
}

// Despawn stars system
pub fn despawn_stars(mut commands: Commands,
    star_query: Query<Entity, With<Star>>) {

    for star_entity in star_query.iter() {
        commands.entity(star_entity).despawn();
    }
}

// Tick spawn timer system
pub fn tick_spawn_star_timer(mut star_spawn_timer: ResMut<StarSpawnTimer>, time: Res<Time>) {
    star_spawn_timer.timer.tick(time.delta());
}

// Spawn stars over time system
pub fn spawn_stars_over_time(mut commands: Commands, 
    window_query: Query<&Window, With<PrimaryWindow>>,
    asset_server: Res<AssetServer>,
    star_spawn_timer: Res<StarSpawnTimer>) {

    // Spawn timer resets, so when it hits 0, make more stars
    if star_spawn_timer.timer.finished() {
        let window = window_query.get_single().unwrap();
        let rand_x = random::<f32>() * window.width();
        let rand_y = random::<f32>() * window.height();

        let sprite_bundle = SpriteBundle {
            transform: Transform::from_xyz(rand_x, rand_y, 0.0),
            texture: asset_server.load("sprites/big_star.png"),
            ..default()
        };
        let new_star = Star {};
        
        commands.spawn(
            (sprite_bundle, new_star)
        );
    }
}