use bevy::prelude::*;

// Score resource
#[derive(Resource)]
pub struct Score {
    pub value: u32,
}

// Implement default trait for score
impl Default for Score {
    fn default() -> Score {
        Score { value: 0 }
    }
}

#[derive(Resource, Debug)]
pub struct HighScores {
    pub scores: Vec<(String, u32)>,
}

// Implement default trait for high score
impl Default for HighScores {
    fn default() -> HighScores {
        HighScores { scores: Vec::new() }
    }
}
