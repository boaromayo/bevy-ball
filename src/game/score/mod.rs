use bevy::prelude::*;

pub mod resources;
mod systems;

use resources::*;
use systems::*;

pub struct ScorePlugin;

// Implement plugin traits for score
impl Plugin for ScorePlugin {
    fn build(&self, app: &mut App) {
        // Load score resources and systems
        app.init_resource::<Score>()
            .init_resource::<HighScores>()
            .add_system(update_score)
            .add_system(update_high_score)
            .add_system(high_scores_updated);
    }
}