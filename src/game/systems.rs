use bevy::prelude::*;

use crate::game::RunState;

pub fn pause_run_state(mut next_run_state: ResMut<NextState<RunState>>) {
    next_run_state.set(RunState::Paused);
}

pub fn resume_run_state(mut next_run_state: ResMut<NextState<RunState>>) {
    next_run_state.set(RunState::Running);
}

pub fn toggle_run_state(mut commands: Commands,
    keyboard_input: Res<Input<KeyCode>>,
    run_state: Res<State<RunState>>) {

    if keyboard_input.just_pressed(KeyCode::P) {
        if run_state.0 == RunState::Running {
            commands.insert_resource(NextState(Some(RunState::Paused)));
            println!("Game paused.");
        }
        if run_state.0 == RunState::Paused {
            commands.insert_resource(NextState(Some(RunState::Running)));
            println!("Game running!");
        }
    }
}