// Call entity modules
pub mod enemy;
mod player;
pub mod score;
pub mod star;
pub mod ui;
mod systems;

// Get plugins
use enemy::EnemyPlugin;
use player::PlayerPlugin;
use score::ScorePlugin;
use star::StarPlugin;
use ui::UiPlugin;
use systems::*;

use crate::events::GameOver;
use crate::AppState;

use bevy::prelude::*;

pub struct GamePlugin;

// Implement plugin trait build() for game plugin
impl Plugin for GamePlugin {
    fn build(&self, app: &mut App) {
        // Add state generator
        app.add_state::<RunState>()
            // Add events
            .add_event::<GameOver>()
            // Add enter system
            .add_system(pause_run_state.in_schedule(OnEnter(AppState::Game)))
            // Add entity plugins
            .add_plugin(EnemyPlugin)
            .add_plugin(PlayerPlugin)
            .add_plugin(ScorePlugin)
            .add_plugin(StarPlugin)
            .add_plugin(UiPlugin)
            // Add systems
            .add_system(toggle_run_state.run_if(in_state(AppState::Game)))
            // Add exit system
            .add_system(resume_run_state.in_schedule(OnExit(AppState::Game)));
    }
}

#[derive(States, Debug, Clone, Copy, Eq, PartialEq, Hash, Default)]
pub enum RunState {
    #[default]
    Running,
    Paused, // Default state
}