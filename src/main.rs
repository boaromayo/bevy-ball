pub mod events;
mod game;
mod main_menu;
mod systems;

// Call plugins
use game::GamePlugin;
use main_menu::MainMenuPlugin;

// Call packages
use systems::*;

use bevy::prelude::*;

fn main() {
    App::new()
    // Add Bevy plugins
    .add_plugins(DefaultPlugins)
    // Add states
    .add_state::<AppState>()
    // Add custom plugins
    .add_plugin(MainMenuPlugin)
    .add_plugin(GamePlugin)
    // Add startup systems
    .add_startup_system(spawn_camera)
    // Add systems, not startup systems, for per-frame update
    // Transition states
    .add_system(transition_to_game_state)
    .add_system(transition_to_main_menu_state)
    // Game over
    .add_system(handle_game_over)
    // Exit game
    .add_system(exit_game)
    .run();
}

// Load in game states
#[derive(States, Debug, Clone, Copy, Eq, PartialEq, Hash, Default)]
pub enum AppState {
    #[default]
    MainMenu, // Default state
    Game,
    GameOver,
}